'use strict';

var Server  = require('./server')
  , Owner   = require('./owner')
  , Players = require('./players')
  , Map     = require('./map')
  , Rates   = require('./rates');

function ServerInfo(obj) {
	if (!obj) {
		this.server   = null;
		this.owner    = null;
		this.players  = null;
		this.monsters = null;
		this.map      = null;
		this.rates    = null;
		this.motd     = null;
		return;
	}
  obj = obj.tsqp;

  this.server   = (obj.serverinfo)  ?  new Server(obj.serverinfo[0].$) : new Server(null);
  this.owner    = (obj.owner)  ? new Owner(obj.owner[0].$) : new Owner(null);
  this.players  = (obj.players)  ?  new Players(obj.players[0].$) : new Players(null);
  this.monsters = (obj.monsters)  ?  parseInt(obj.monsters[0].$.total) : null;
  this.map      = (obj.map)  ?  new Map(obj.map[0].$) : new Map(null);
  this.rates    = (obj.rates)  ?  new Rates(obj.rates[0].$) : new Rates(null);
  this.motd     = (obj.motd)  ?  obj.motd[0] : null;
}

module.exports = ServerInfo;
