'use strict';

function Owner(obj) {
	if(obj){
		this.name  = (obj.name) ? obj.name:null;
		this.email = (obj.email) ? obj.email:null;
	}
	else {
		this.name  = null;
		this.email = null;
	}
}

module.exports = Owner;
