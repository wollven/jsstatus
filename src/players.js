'use strict';

function Players(obj) {
	if (obj){
		this.online = (obj.online) ? parseInt(obj.online):null;
		this.max    = (obj.max) ? parseInt(obj.max):null;
		this.peak   = (obj.peak) ? parseInt(obj.peak):null;
	}
	else {
		this.online = null;
		this.max    = null;
		this.peak   = null;
	}
}

module.exports = Players;
