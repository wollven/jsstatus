'use strict';

function Map(obj) {
	if(obj){
		this.name      = (obj.name) ? obj.name:null;
		this.author    = (obj.author) ? obj.author:null;
		this.width     = (obj.width) ? parseInt(obj.width):null;
		this.height    = (obj.height) ? parseInt(obj.height):null;
	}
	else{
		this.name      = null;
		this.author    = null;
		this.width     = null;
		this.height    = null;
	}
}

module.exports = Map;
