var events = require('events'), net = require('net'), util = require('util');
var Enum = require('simple-enum');

function Communication(host, port) {
	connectionStates = Enum(['Disconnected', 'Connected']);
	this.client 			= null;
	this.messagesReceived 	= 0;
	this.bytesReceived		= 0;

	var host	= host;
	var port 	= port;
	var connectionState = connectionStates.Disconnected;

	events.EventEmitter.call(this);

	var that = this;

	this.connect = function() {
		if(connectionState === connectionStates.Disconnected) {
			var origin = this;
			
				this.client = net.connect({ port: port, host: host }, function() {
					connectionState = connectionStates.Connected;
					that.client.setKeepAlive(true);
					that.client.setNoDelay(true);
					that.client.setTimeout(5000, function(){
						that.client.end();
						console.log('[Communication::connect][Info]: Connection TIMEDOUT to `' + host + ':' + port + '`.');
						origin.emit('timedout', { code: 'ECONNTIMEDOUT' });
					});

					origin.emit('connected');
					console.log('[Communication::connect][Info]: Connected to `' + host + ':' + port + '`.');
				});
			
			this.client.on('end', function() {
				connectionState = connectionStates.Disconnected;
				console.log('[Communication::connect][Info]: Connection lost to `' + host + ':' + port + '`.');
				origin.emit('lost', { code: 'ECONNLOST' });
			})
			this.client.on('error', function(e) {
				console.log('[Communication::connect][Error] with to `' + host + ':' + port + '`:', e);
				origin.emit('error', e);
			});
		} else
			console.log('[Communication::connect][Error]: Connection is already made to `' + host + ':' + port + '`.');
	}

	this.send = function(data) {
		if(connectionState === connectionStates.Connected) {
			var origin = this;
			this.client.on('data', function(bytes) {
				origin.emit('receivedBytes', bytes);
				that.client.end();
				console.log("[Communication::connect][Info]: Message received. Message num:" + origin.messagesReceived);
			});
			this.client.write(data);
			
		} else
			console.log('[Communication::send][Error]: Can not send data - client is disconnected: `' + host + ':' + port + '`.');
	}

	this.end = () => {
		this.client.destroy();
	}
}
util.inherits(Communication, events.EventEmitter);
module.exports = Communication;