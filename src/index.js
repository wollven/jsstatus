'use strict';
var Q = require('q')
, MsgBuffer = require('./MsgBuffer.js'),
	Communication = require('./Communication.js');

const EventEmitter = require('events');

var eventEmitter = new EventEmitter();



function ServerStatus(host, port, checkPlayers = false) {
  this.host     = host;
  this.port     = port || 7171;
  this.deferred = Q.defer();
  this.OnlinePlayers = 0,
  this.PlayersList=[];
  this.Data={
	tsqp : {
	serverinfo : [{$ : null}],
	owner : [{$ : null}],
	players : [{$ : null}],
	map : [{$ : null}],
	motd : [null],
	rates : null,
	monsters : null,
	}
};
this.server  = {};
this.owner = {};
this.players = {};
this.map = {};
this.motd;
this.messageSent = false;
  this.online = false;
	var context = this;
	var players = undefined;

	if(!checkPlayers)
  var info = this.execute();
	else
	{
		  var info = this.getAllInfo();//this.getOnlinePlayers();
	}
  /*info.then(function(data) {
		context.getOnlinePlayers();
	},context)
	.fail(function(err) {
		console.log(err);
	});	
	console.log(this.OnlinePlayers);*/
  return info;
}

ServerStatus.prototype.execute = function() {
  var 
   status = undefined
  , self   = this
  ;

	var sendBuffer = new MsgBuffer(new Buffer([255, 255]));	
	sendBuffer.putString('info');
	

	var communication = new Communication(self.host, self.port);
	var timeout = 
	setTimeout(function() {

		communication.end();
		self.deferred.reject(new Error("Timed out"));
	  
	}, 10000);

	communication.on('connected', function () {
		eventEmitter.emit("connected");
		communication.send(sendBuffer.getBuffer(true));
	})

	communication.on('receivedBytes', function(data) {
		clearTimeout(timeout);
		if (typeof data != undefined && data && data.toString('utf8').search("xml") > -1) {
			var xml  = require('xml2js'),
			parser = xml.Parser(),
			Status = require('./status');
			parser.parseString(data.toString('utf8'), function(err, data) {
			if(err) {
				self.deferred.reject(new Error(err));
			}
			
			status = new Status(data);
			self.online = true;
			self.deferred.resolve(status);
			//console.log(status);
			
			});
		}
	});

	communication.on('error', function(e) {
		self.deferred.reject(e)
	});

	communication.on('lost', function(e) {
		self.deferred.reject(e);
	});
	
	communication.connect();
  
	return self.deferred.promise;
};

ServerStatus.prototype.getAllInfo = function() {
	/* How TFS handles this: 
	- FIRST 2 BYTES: int32_t size = (int32_t)(m_buffer[0] | m_buffer[1] << 8); // Gets size of message as int32 but
		probably uint16 should be send as length
	- 3 BYTE: uint8_t type = msg.get<char>(); // Gets the type as uint8, then
		there is a switch:
		a) 3 BYTE = 0xFF
			4 BYTE = 0x01
		b) 3 BYTE = 0x01 - REQUEST_BASIC_SERVER_INFO
	*/
	var Player = require('./Player.js') , self   = this;	
	var Status = require('./status');

		
	
	const REQUEST_BASIC_SERVER_INFO	 = 0x01,
		REQUEST_SERVER_OWNER_INFO	 = 0x02,
		REQUEST_MISC_SERVER_INFO	 = 0x04,
		REQUEST_PLAYERS_INFO		 = 0x08,
		REQUEST_SERVER_MAP_INFO		 = 0x10,
		REQUEST_EXT_PLAYERS_INFO	 = 0x20,
		REQUEST_PLAYER_STATUS_INFO	 = 0x40,
		REQUEST_SERVER_SOFTWARE_INFO = 0x80;

	var sendBuffer = new MsgBuffer(new Buffer.from([0xFF, 0x01, REQUEST_BASIC_SERVER_INFO | REQUEST_SERVER_OWNER_INFO | REQUEST_MISC_SERVER_INFO | REQUEST_PLAYERS_INFO 
		| REQUEST_SERVER_MAP_INFO | REQUEST_EXT_PLAYERS_INFO | REQUEST_SERVER_SOFTWARE_INFO]));

	var communication = new Communication(self.host, self.port); 
	var timeout =
	setTimeout(function() {

		communication.end();
		self.deferred.reject(new Error("Timed out"));
	  
	}, 5000);

	communication.on('connected', function() {
		communication.send(sendBuffer.getBuffer(true));
		origin.messageSent = true;
	});

	var origin = this;
	var msg = undefined;
		var hasPlayers = false;
		var oldPos = 0;
		var received = false;
		
	communication.on('receivedBytes', function(bytes) {
		received = true;
		clearTimeout(timeout);
		this.bytesReceived += bytes.length;
		this.messagesReceived++;	
		
		
		if(this.messagesReceived == 1) {
			msg = new MsgBuffer(bytes);
			msg.CompleteLength = msg.getUint16();
			oldPos = msg.position;
			if(msg.verifyNextByte(0x10)){
				var stringLenght 	= msg.getUint16();
				origin.server.servername = msg.getString(stringLenght, 'UTF8');
				stringLenght 	= msg.getUint16();
				origin.server.ip = msg.getString(stringLenght, 'UTF8');
				stringLenght 	= msg.getUint16();
				origin.server.port = msg.getString(stringLenght, 'UTF8');
			}
			else {
				msg.position = oldPos;
			}	

			oldPos = msg.position;
			if(msg.verifyNextByte(0x11)){
			stringLenght 	= msg.getUint16();
			origin.owner.name = msg.getString(stringLenght, 'UTF8');
			stringLenght 	= msg.getUint16();
			origin.owner.email = msg.getString(stringLenght, 'UTF8');
			}
			else {
				msg.position = oldPos;
			}

			oldPos = msg.position;
			if(msg.verifyNextByte(0x12)){
			stringLenght 	= msg.getUint16();
			origin.motd = msg.getString(stringLenght, 'UTF8');
			stringLenght 	= msg.getUint16();
			origin.server.location = msg.getString(stringLenght, 'UTF8');
			stringLenght 	= msg.getUint16();
			origin.server.url = msg.getString(stringLenght, 'UTF8');
			origin.server.uptime = msg.getUint64();
			}
			else {
				msg.position = oldPos;
			}

			oldPos = msg.position;
			if(msg.verifyNextByte(0x20)){
				origin.players.online = msg.getInt32();
				origin.players.max = msg.getInt32();
				origin.players.peak = msg.getInt32();
			}
			else {
				msg.position = oldPos;
			}

			oldPos = msg.position;
			if(msg.verifyNextByte(0x30)){
			stringLenght 	= msg.getUint16();
			origin.map.name = msg.getString(stringLenght, 'UTF8');
			stringLenght 	= msg.getUint16();
			origin.map.author = msg.getString(stringLenght, 'UTF8');
			origin.map.width = msg.getUint16();
			origin.map.height = msg.getUint16();
			}
			else {
				msg.position = oldPos;
			}
			//console.log(map);
			oldPos = msg.position;
			hasPlayers = msg.verifyNextByte(0x21);
			if (hasPlayers)
			origin.OnlinePlayers = msg.getInt32();
			else
			msg.position = oldPos;
		} else {				
			msg.storeBuffer(bytes);
		}
		if(msg.CompleteLength == (this.bytesReceived - 2)) {
			//console.log(msg.CompleteLength);
			//console.log(msg.getStoredBuffersLength())
			msg.loadBytes();
			console.log('[Status::getOnlinePlayers][Info]: Received all bytes(' + this.bytesReceived + '). ' + self.host + ':' + self.port + '');
			console.log('[Status::getOnlinePlayers][Info]: Complete length:(' + msg.CompleteLength + '). ' + self.host + ':' + self.port + '');
			console.log('[Status::getOnlinePlayers][Info]: Online Players:(' + origin.OnlinePlayers + '). ' + self.host + ':' + self.port + '');
			var emitBeyond = false;
			if (hasPlayers){
			while(origin.PlayersList.length < origin.OnlinePlayers) {
				if (msg.position >= msg.CompleteLength) {
					emitBeyond = true;
					console.log('[Status::getOnlinePlayers][Info]: Beyond max buffer, breaking')
					break;
				}
				var nickLength 	= msg.getUint16();	
				
				//console.log(i++);
				if (msg.position >= msg.CompleteLength) {
					emitBeyond = true;
					console.log('[Status::getOnlinePlayers][Info]: Beyond max buffer, breaking')
					break;
				}
				var name =	msg.getString(nickLength, 'UTF8');	
				if (msg.position >= msg.CompleteLength) {
					emitBeyond = true;
					console.log('[Status::getOnlinePlayers][Info]: Beyond to max buffer, breaking')
					break;
				}
				var level = msg.getInt32();
				var player 		= new Player(name, level);
				origin.PlayersList.push(player);
			}
			}
			console.log('[Status::getOnlinePlayers][Info]: Got all players(' + origin.PlayersList.length + '). ' + self.host + ':' + self.port + '');

			if (emitBeyond){
				eventEmitter.emit("beyond", origin.OnlinePlayers, origin.PlayersList.length);
			}

			if ( msg.position < msg.CompleteLength ){
				oldPos = msg.position;
				if(msg.verifyNextByte(0x23)){
				var stringLenght 	= msg.getUint16();
				origin.server.server = msg.getString(stringLenght, 'UTF8');
				stringLenght 	= msg.getUint16();
				origin.server.version = msg.getString(stringLenght, 'UTF8');
				stringLenght 	= msg.getUint16();
				origin.server.client = msg.getString(stringLenght, 'UTF8');
				}
			}
			
			origin.Data = {
				tsqp : {
				serverinfo : [{$ : origin.server}],
				owner : [{$ : origin.owner}],
				players : [{$ : origin.players}],
				map : [{$ : origin.map}],
				motd : [origin.motd],
				rates : null,
				monsters : null,
				}
			}

			var serverStatus = new Status(origin.Data);
			console.log(serverStatus);			
			//console.log(origin.PlayersList);

			//rigin.emit('players');
			self.online = true;
			self.deferred.resolve({info:serverStatus, players:origin.PlayersList});
		}
	});

	communication.on('timedout', function(e) {
		console.log(self.host + ':' + self.port + 'Timedout during connection, trying to salvage received messages...');
		origin.Data = {
			tsqp : {
			serverinfo : [{$ : origin.server}],
			owner : [{$ : origin.owner}],
			players : [{$ : origin.players}],
			map : [{$ : origin.map}],
			motd : [origin.motd],
			rates : null,
			monsters : null,
			}
		};
		var serverStatus = new Status(origin.Data);
			console.log(serverStatus);	
			console.log(origin.PlayersList);	

			self.online = true;
			self.deferred.resolve({info:serverStatus, players:origin.PlayersList});
		//self.deferred.reject(e);
	});

	communication.on('error', function(e) {
		self.deferred.reject(e);
	});

	communication.on('lost', function(e) {
		if (!origin.messageSent)
		console.log('Connection lost before receiving message (timeout?) - ' + self.host + ':' + self.port + '');
		self.deferred.reject(e);
	});

	communication.connect();
	
	return self.deferred.promise;
};

//ServerStatus.prototype.getAllInfo(); 
//new ServerStatus('taleon.online',6742,true); 

module.exports = {
    ServerStatus: ServerStatus,
    eventEmitter: eventEmitter
}