'use strict';

function Rates(obj) {
	if (obj) {
		this.experience = (obj.experience) ? parseInt(obj.experience) : null;
		this.magic      = (obj.magic) ? parseInt(obj.magic) : null;
		this.skill      = (obj.skill) ? parseInt(obj.skill) : null;
		this.loot       = (obj.loot) ? parseInt(obj.loot) : null;
		this.spawn      = (obj.spawn) ? parseInt(obj.spawn) : null;
	}
	else
	{
		this.experience = null;
		this.magic      = null;
		this.skill      = null;
		this.loot       = null;
		this.spawn      = null;
	}
}

module.exports = Rates;
