'use strict';

function Server(obj) {
if (obj){
  this.uptime         = (obj.uptime) ? parseInt(obj.uptime):null;
  this.ip             = (obj.ip) ? obj.ip:null;
  this.servername     = (obj.servername) ? obj.servername:null;
  this.port           = (obj.port) ? parseInt(obj.port):null;
  this.location       = (obj.location) ? obj.location:null;
  this.url            = (obj.url) ? obj.url:null;
  this.server         = (obj.server) ? obj.server:null;
  this.version        = (obj.version) ? obj.version:null;
  this.client         = (obj.client) ? obj.client:null;
}
else {
  this.uptime         = null;
  this.ip             = null;
  this.servername     = null;
  this.port           = null;
  this.location       = null;
  this.url            = null;
  this.server         = null;
  this.version        = null;
  this.client         = null;
}
}

module.exports = Server;
